package com.example.android.guesstheword.screens.score

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ScoreViewModel(finalScore: Int): ViewModel() {
    private val _eventGameFinish = MutableLiveData<Boolean>()
    val eventGameFinish: LiveData<Boolean>
        get() = _eventGameFinish

    val score = finalScore

    init {
        _eventGameFinish.value = false
        Log.i("ScoreViewModel", "Final score is $finalScore.")
    }

    fun onRestartComplete() {
        _eventGameFinish.value = false
    }

    fun onPlayAgain() {
        _eventGameFinish.value = true
    }
}